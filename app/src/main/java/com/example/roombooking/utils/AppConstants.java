package com.example.roombooking.utils;

public interface AppConstants {
    /**
     * Network timeouts
     */
    byte CONNECT_TIMEOUT = 120;
    byte WRITE_TIMEOUT = 120;
    byte READ_TIMEOUT = 120;

    String A_CORP_BASE_API_URL = "https://acorp.dac.eu/roombooking_app/";
    String B_CORP_BASE_API_URL = "https://bcorp.dac.eu/roombooking_app/";

    String BASE_URL_PATH = "https://acorp.dac.eu/roombooking_app/";

    String PREFS_FILE_NAME = "prefs";

}
