package com.example.roombooking.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.TypedValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;

public class AppUtils {

    public static int convertDpToPx(Context context, int dp) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, context.getResources().getDisplayMetrics());
    }

    public static long convertDateToUnitStamp(String date) {
        @SuppressLint("SimpleDateFormat") final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
        Date dates = null;
        try {
            dates = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dates.getTime();
    }

    @SuppressLint("DefaultLocale")
    public static String getTime() {
        Calendar calendar = Calendar.getInstance();
        int hour24hrs = calendar.get(Calendar.HOUR_OF_DAY);
        int hour12hrs = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);

        return String.format("%d:%d", hour24hrs, minutes);
    }

    @SuppressLint("DefaultLocale")
    public static Map<String, Integer> getTimeDifference(String startTime, String endTime) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        int hours = 0, mins = 0;
        try {
            Date date1 = format.parse(startTime);
            Date date2 = format.parse(endTime);
            long millis = date2.getTime() - date1.getTime();
            hours = (int) (millis / (1000 * 60 * 60));
            mins = (int) ((millis / (1000 * 60)) % 60);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Map<String, Integer> result = new HashMap<>();
        result.put("hours", hours);
        result.put("mins", mins);

        return result;
    }


}
