package com.example.roombooking;

import android.app.Application;

import com.example.roombooking.di.AppComponent;
import com.example.roombooking.di.DaggerAppComponent;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class App extends Application {

    private static final String TAG = App.class.getName();

    private static AppComponent sAppComponent;

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());

        sAppComponent = DaggerAppComponent.builder().app(this).build();
        sAppComponent.inject(this);
    }
}
