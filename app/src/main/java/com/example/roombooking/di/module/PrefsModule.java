package com.example.roombooking.di.module;

import android.content.Context;

import com.example.roombooking.data.prefs.PrefsService;
import com.example.roombooking.data.prefs.PrefsServiceImpl;
import com.example.roombooking.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

@Module
public class PrefsModule implements AppConstants {

    @Provides
    @Singleton
    static PrefsService providePrefsService(Context context) {
        return new PrefsServiceImpl(context, PREFS_FILE_NAME);
    }
}
