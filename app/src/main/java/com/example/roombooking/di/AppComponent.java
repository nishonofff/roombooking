package com.example.roombooking.di;


import com.example.roombooking.App;
import com.example.roombooking.di.module.AppModule;
import com.example.roombooking.ui.add_booking.AddBookingPresenter;
import com.example.roombooking.ui.add_workspace.AddWorkspacePresenter;
import com.example.roombooking.ui.main.bookings.FindRoomPresenter;
import com.example.roombooking.ui.main.home.HomePresenter;
import com.example.roombooking.ui.main.home.corp_a.ACorpPresenter;
import com.example.roombooking.ui.main.home.corp_b.BCorpPresenter;
import com.example.roombooking.ui.splash_screen.SplashScreenPresenter;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(App app);

    void inject(FindRoomPresenter presenter);

    void inject(HomePresenter presenter);

    void inject(ACorpPresenter presenter);

    void inject(BCorpPresenter presenter);

    void inject(AddWorkspacePresenter presenter);

    void inject(AddBookingPresenter presenter);

    void inject(SplashScreenPresenter presenter);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(App app);

        AppComponent build();
    }
}
