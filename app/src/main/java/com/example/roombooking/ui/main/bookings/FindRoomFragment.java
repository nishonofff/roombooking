package com.example.roombooking.ui.main.bookings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.example.roombooking.ui.widgets.adapters.RoomsAdapter;
import com.example.roombooking.ui.widgets.adapters.WorkspaceAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FindRoomFragment extends MvpAppCompatFragment implements RoomsAdapter.RoomItemClickListener,
        FindRoomView {

    @BindView(R.id.rooms_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    View mProgressView;

    @InjectPresenter
    FindRoomPresenter mFindRoomPresenter;

    private WorkspaceAdapter mAdapter;

    public static FindRoomFragment newInstance() {
        FindRoomFragment fragment = new FindRoomFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFindRoomPresenter.getWorkspaceList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_find_room, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mAdapter = new WorkspaceAdapter(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    @Override
    public void showNoInternetConnection() {
    }

    @Override
    public void setWorkspaceList(List<WorkspaceResponse.Workspace> workspaceList) {
        if (workspaceList.size() == 0) {
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.setWorkspaceList(workspaceList);
            mRecyclerView.setAdapter(mAdapter);
        }
    }


    @Override
    public void loadRoomListError() {
    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void onRoomItemClick(int position, RoomResponse.Room roomItem) {

    }
}
