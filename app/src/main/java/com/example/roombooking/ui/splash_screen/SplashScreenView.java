package com.example.roombooking.ui.splash_screen;

import com.arellomobile.mvp.MvpView;

public interface SplashScreenView extends MvpView {

    void showNoInternetConnection();

    void setUserStatus(String workspaces);

}
