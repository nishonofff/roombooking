package com.example.roombooking.ui.main.home;

import com.arellomobile.mvp.MvpView;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;

import java.util.List;

public interface HomeView extends MvpView {

    void showNoInternetConnection();

    void setWorkspaces(String workspaces, List<WorkspaceResponse.Workspace> workspaceList);

    void loadRoomListError();

    void showProgress();

    void hideProgress();


}
