package com.example.roombooking.ui.add_booking;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.roombooking.App;
import com.example.roombooking.data.network.ApiService;
import com.example.roombooking.data.network.entity.AttendeeItem;
import com.example.roombooking.data.network.entity.BookingResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.example.roombooking.utils.AppUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;


/**
 * The type Add booking presenter.
 */
@InjectViewState
public class AddBookingPresenter extends MvpPresenter<AddBookingView> {

    private static final String TAG = AddBookingPresenter.class.getName();

    /**
     * The ApiService.
     */
    @Inject
    ApiService mApiService;

    /**
     * Instantiates a new Add booking presenter.
     */
    AddBookingPresenter() {
        App.getAppComponent().inject(this);
    }


    /**
     * Add booking
     *
     * @param workspace        the workspace
     * @param date             the date
     * @param startTime        the start time
     * @param endTime          the end time
     * @param title            the title
     * @param description      the description
     * @param roomNumber       the room number
     * @param attendeeItemList the attendee item list
     */
    @SuppressLint("CheckResult")
    void addBooking(String workspace, String date, String startTime, String endTime, String title, String description, String roomNumber, List<AttendeeItem> attendeeItemList) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        Map<String, Object> body = new HashMap<>();

        Map<String, Object> booking = new HashMap<>();

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date startDate = null, endDate = null;
        try {
            startDate = format.parse(startTime);
            endDate = format.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long startTimestamp = startDate.getTime();
        long endTimestamp = endDate.getTime();

        booking.put("date", AppUtils.convertDateToUnitStamp(date.replace(".", "/")));
        booking.put("time_start", startTimestamp);
        booking.put("time_end", endTimestamp);
        booking.put("title", title);
        booking.put("description", description);
        booking.put("room", roomNumber);

        List<Map<String, String>> users = new ArrayList<>();

        for (AttendeeItem attendeeItem : attendeeItemList) {
            Map<String, String> user = new HashMap<>();
            user.put("name", attendeeItem.name);
            user.put("email", attendeeItem.email);
            user.put("number", attendeeItem.phoneNumber);

            users.add(user);
        }

        body.put("booking", booking);

        body.put("passes", users);

        if (workspace.equals("A")) {
            mApiService.addBookingFromACorp(body).subscribe((BookingResponse response) -> {
                        getViewState().hideProgress();
                        if (response.success)
                            getViewState().addBookingSuccess();
                        if (response.error != null)
                            getViewState().addBookingError(response.error.text);
                    },
                    (Throwable throwable) -> {
                        getViewState().hideProgress();
                        getViewState().addBookingError("Oops! Something went wrong. Please, check your connections and try again");
                    });
        }
        if (workspace.equals("B")) {
            mApiService.addBookingFromBCorp(body).subscribe((BookingResponse response) -> {
                        getViewState().hideProgress();
                        if (response.success)
                            getViewState().addBookingSuccess();
                        if (response.error != null)
                            getViewState().addBookingError(response.error.text);
                    },
                    (Throwable throwable) -> {
                        getViewState().hideProgress();
                        getViewState().addBookingError("Oops! Something went wrong. Please, check your connections and try again");
                    });
        }
    }
}
