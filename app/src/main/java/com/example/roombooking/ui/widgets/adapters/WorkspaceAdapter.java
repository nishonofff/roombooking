package com.example.roombooking.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.roombooking.utils.AppConstants.BASE_URL_PATH;

public class WorkspaceAdapter extends RecyclerView.Adapter<WorkspaceAdapter.AbstractViewHolder> {

    private Context mContext;

    private static WorkspaceItemClickListener mListener;

    private List<WorkspaceResponse.Workspace> mWorkspaceList;

    public WorkspaceAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(WorkspaceItemClickListener listener) {
        mListener = listener;
    }

    public void setWorkspaceList(List<WorkspaceResponse.Workspace> workspaceList) {
        mWorkspaceList = workspaceList;
    }

    @NonNull
    @Override
    public WorkspaceAdapter.AbstractViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new WorkspaceViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.workspace_item, parent, false));
        } else {
            return new AddWorkspaceViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.add_workspace_item, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public void onBindViewHolder(@NonNull AbstractViewHolder viewHolder, int position) {
        viewHolder.initView(mWorkspaceList.get(position));

    }

    @Override
    public int getItemCount() {
        if (mWorkspaceList != null)
            return mWorkspaceList.size();
        else return 0;
    }

    public interface WorkspaceItemClickListener {
        void onRoomItemClick(int position, RoomResponse.Room roomItem);
    }

    abstract class AbstractViewHolder extends RecyclerView.ViewHolder {

        AbstractViewHolder(View itemView) {
            super(itemView);
        }

        abstract void initView(WorkspaceResponse.Workspace workspaceItem);

    }

    class WorkspaceViewHolder extends AbstractViewHolder {

        @BindView(R.id.workspace_title)
        TextView mWorkspaceTitle;
        @BindView(R.id.workspace_item_image)
        RoundedImageView mCardImageView;

        private Context mContext;

        public WorkspaceViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mContext = itemView.getContext();
        }

        @Override
        void initView(WorkspaceResponse.Workspace workspaceItem) {
            mWorkspaceTitle.setText(workspaceItem.title);

            Picasso.with(mContext)
                    .load(BASE_URL_PATH + workspaceItem.icon)
                    .into(mCardImageView);
        }
    }

    class AddWorkspaceViewHolder extends AbstractViewHolder {

        @BindView(R.id.radio_button)
        RadioButton mRadioButton;
        @BindView(R.id.workspace_title)
        TextView mWorkspaceTitle;
        @BindView(R.id.workspace_item_image)
        RoundedImageView mCardImageView;
        private Context mContext;

        public AddWorkspaceViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mContext = itemView.getContext();
        }

        @Override
        void initView(WorkspaceResponse.Workspace workspaceItem) {

            mWorkspaceTitle.setText(workspaceItem.title);

            Picasso.with(mContext)
                    .load(BASE_URL_PATH + workspaceItem.icon)
                    .into(mCardImageView);

        }
    }
}
