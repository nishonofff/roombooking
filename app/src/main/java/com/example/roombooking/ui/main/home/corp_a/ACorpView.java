package com.example.roombooking.ui.main.home.corp_a;

import com.arellomobile.mvp.MvpView;
import com.example.roombooking.data.network.entity.RoomResponse;

import java.util.List;

public interface ACorpView extends MvpView {

    void showNoInternetConnection();

    void setRoomList(List<RoomResponse.Room> roomList);

    void loadRoomListError();

    void showProgress();

    void hideProgress();


}
