package com.example.roombooking.ui.add_workspace;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.example.roombooking.ui.main.MainActivity;
import com.example.roombooking.ui.widgets.adapters.WorkspaceAdapter;
import com.example.roombooking.ui.widgets.dialogs.AlertDialogs;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddWorkspaceActivity extends MvpAppCompatActivity implements AddWorkspaceView {

    @BindView(R.id.workspace_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.fab_add_workspace)
    FloatingActionButton mFab;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    /**
     * AddWorkspacePresenter
     */
    @InjectPresenter
    AddWorkspacePresenter mAddWorkspacePresenter;

    private AlertDialog mAlertDialog;

    private WorkspaceAdapter mAdapter;
    private AutoCompleteTextView mWorkspaceName;

    String[] mHints = new String[2];
    private List<WorkspaceResponse.Workspace> mWorkspaces;
    private List<WorkspaceResponse.Workspace> mWorkspaceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_workspace);

        ButterKnife.bind(this);

        mAddWorkspacePresenter.getWorkspaces();


        mAdapter = new WorkspaceAdapter(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onResume() {
        super.onResume();
        if (mWorkspaceList.size() == 2) {
            mFab.setVisibility(View.GONE);
        }
    }

    /**
     * On add workspace clicked.
     */
    @OnClick(R.id.fab_add_workspace)
    void onAddWorkspaceClicked() {
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_workspace_view, null);
        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (this, android.R.layout.select_dialog_item, mHints);

        mWorkspaceName = dialogView.findViewById(R.id.workspace_name);
        mAlertDialog = new AlertDialog.Builder(this, R.style.AlertDialog)
                .setTitle(getResources().getString(R.string.title_add_workspace))
                .setPositiveButton("Add", (dialog, which) -> {
                    for (WorkspaceResponse.Workspace workspace : mWorkspaces) {
                        if (workspace.title.equals(mWorkspaceName.getText().toString())) {
                            if (!mWorkspaceList.contains(workspace)) {
                                mWorkspaceList.add(workspace);
                                mAdapter.setWorkspaceList(mWorkspaceList);
                                mAdapter.notifyItemChanged(mWorkspaceList.size());
                            } else
                                AlertDialogs.showErrorDialog(this, getResources().getString(R.string.msg_workspace_exist));
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .setView(dialogView).create();

        mAlertDialog.show();

        mWorkspaceName.setThreshold(1);
        mWorkspaceName.setAdapter(adapter);
    }

    /**
     * On continue clicked.
     */
    @OnClick(R.id.continue_button)
    void onContinueClicked() {

        if (mWorkspaceList.size() == 0)
            AlertDialogs.showErrorDialog(this, getResources().getString(R.string.msg_error_add_workspace));
        else {

            mAddWorkspacePresenter.saveWorkspaces(mWorkspaceList);

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showErrorDialog(this, getResources().getString(R.string.msg_error));
    }

    @Override
    public void setWorkspaceList(List<WorkspaceResponse.Workspace> workspaceList) {
        mWorkspaces = workspaceList;

        int i = 0;
        for (WorkspaceResponse.Workspace workspace : workspaceList) {
            mHints[i++] = workspace.title;
        }
    }

    @Override
    public void loadRoomListError() {
        AlertDialogs.showErrorDialog(this, getResources().getString(R.string.msg_error));
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }
}
