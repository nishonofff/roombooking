package com.example.roombooking.ui.main.home;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.roombooking.App;
import com.example.roombooking.data.network.ApiService;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.example.roombooking.data.prefs.PrefsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView> {

    private static final String TAG = HomePresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    List<WorkspaceResponse.Workspace> mWorkspaceList = new ArrayList<>();

    HomePresenter() {
        App.getAppComponent().inject(this);
    }


    @SuppressLint("CheckResult")
    void getWorkspaces() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        mApiService.getWorkspaceFromACorp().subscribe((WorkspaceResponse.Workspace workspace) -> {
                    mWorkspaceList.add(workspace);
                    getWorkspaceFromBCorp();
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });
    }

    @SuppressLint("CheckResult")
    void getWorkspaceFromBCorp() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        mApiService.getWorkspaceFromBCorp().subscribe((WorkspaceResponse.Workspace workspace) -> {
                    getViewState().hideProgress();
                    mWorkspaceList.add(workspace);
                    getViewState().setWorkspaces(mPrefsService.getWorkspace("WORKSPACE"), mWorkspaceList);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });
    }


}
