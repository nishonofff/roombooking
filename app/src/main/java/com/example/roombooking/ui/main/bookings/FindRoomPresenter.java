package com.example.roombooking.ui.main.bookings;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.roombooking.App;
import com.example.roombooking.data.network.ApiService;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

@InjectViewState
public class FindRoomPresenter extends MvpPresenter<FindRoomView> {

    private static final String TAG = FindRoomPresenter.class.getName();

    @Inject
    ApiService mApiService;

    FindRoomPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getWorkspaceList() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        Map<String, String> body = new HashMap<>();
        body.put("date", "today");


        List<WorkspaceResponse.Workspace> workspaceList = new ArrayList<>();

        mApiService.getWorkspaceFromACorp().subscribe((WorkspaceResponse.Workspace workspace) -> workspaceList.add(workspace),
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });

        mApiService.getWorkspaceFromBCorp().subscribe((WorkspaceResponse.Workspace workspace) -> {
                    getViewState().hideProgress();
                    workspaceList.add(workspace);
                    getViewState().setWorkspaceList(workspaceList);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });
    }


}
