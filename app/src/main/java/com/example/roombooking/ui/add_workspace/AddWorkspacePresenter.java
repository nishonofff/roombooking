package com.example.roombooking.ui.add_workspace;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.roombooking.App;
import com.example.roombooking.data.network.ApiService;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.example.roombooking.data.prefs.PrefsService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * The type Add workspace presenter.
 */
@InjectViewState
public class AddWorkspacePresenter extends MvpPresenter<AddWorkspaceView> {

    private static final String TAG = AddWorkspacePresenter.class.getName();

    /**
     * The api service.
     */
    @Inject
    ApiService mApiService;

    /**
     * The prefs service.
     */
    @Inject
    PrefsService mPrefsService;

    /**
     * Instantiates a new Add workspace presenter.
     */
    AddWorkspacePresenter() {
        App.getAppComponent().inject(this);
    }


    /**
     * Gets workspaces.
     */
    @SuppressLint("CheckResult")
    void getWorkspaces() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        List<WorkspaceResponse.Workspace> workspaceList = new ArrayList<>();

        mApiService.getWorkspaceFromACorp().subscribe((WorkspaceResponse.Workspace workspace) -> workspaceList.add(workspace),
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });

        mApiService.getWorkspaceFromBCorp().subscribe((WorkspaceResponse.Workspace workspace) -> {
                    getViewState().hideProgress();
                    workspaceList.add(workspace);
                    getViewState().setWorkspaceList(workspaceList);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                });
    }

    /**
     * Save workspaces.
     *
     * @param workspaces the workspaces
     */
    @SuppressLint("CheckResult")
    void saveWorkspaces(List<WorkspaceResponse.Workspace> workspaces) {
        if (workspaces.size() == 2)
            mPrefsService.setWorkspace("WORKSPACE", "AB");
        else if (workspaces.get(0).title.equals("Princess Cupcakes"))
            mPrefsService.setWorkspace("WORKSPACE", "A");
        else if (workspaces.get(0).title.equals("Urban Brews"))
            mPrefsService.setWorkspace("WORKSPACE", "B");
    }

}
