package com.example.roombooking.ui.splash_screen;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.roombooking.App;
import com.example.roombooking.data.network.ApiService;
import com.example.roombooking.data.prefs.PrefsService;

import javax.inject.Inject;

@InjectViewState
public class SplashScreenPresenter extends MvpPresenter<SplashScreenView> {
    private static final String TAG = SplashScreenPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    SplashScreenPresenter() {
        App.getAppComponent().inject(this);
    }


    @SuppressLint("CheckResult")
    void checkUserStatus() {
            getViewState().setUserStatus(String.valueOf(mPrefsService.getWorkspace("WORKSPACE")));

    }

}
