package com.example.roombooking.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.RoomResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RoomsAdapter extends RecyclerView.Adapter<RoomsAdapter.ViewHolder> {

    private Context mContext;

    private static RoomItemClickListener mItemClickListener;
    private List<RoomResponse.Room> mRoomList;

    public RoomsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(RoomItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setRoomList(List<RoomResponse.Room> roomList) {
        mRoomList = roomList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.room_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setRoomItem(mRoomList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mRoomList != null)
            return mRoomList.size();
        else return 0;
    }

    public interface RoomItemClickListener {
        void onRoomItemClick(int position, RoomResponse.Room roomItem);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.room_title)
        TextView mRoomTitle;

        @BindView(R.id.room_address)
        TextView mRoomAddress;

        @BindView(R.id.laptop_view)
        View mLaptopView;
        @BindView(R.id.laptops_view)
        View mLaptopsView;
        @BindView(R.id.fridge_view)
        View mFridgeView;
        @BindView(R.id.hifi_system_view)
        View mHifiSystemView;
        @BindView(R.id.projector_view)
        View mProjectorView;
        @BindView(R.id.flat_screen_view)
        View mFlatScreenView;
        @BindView(R.id.lego_set_view)
        View mLegoSetView;
        @BindView(R.id.whiteboard_view)
        View mWhiteboardView;

        RoomItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, RoomItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setRoomItem(RoomResponse.Room roomItem) {
            mRoomTitle.setText(String.format("Room %s", roomItem.name));
            mRoomAddress.setText(roomItem.location);

            if (roomItem.equipment.contains("Laptop"))
                mLaptopView.setVisibility(View.VISIBLE);
            else mLaptopView.setVisibility(View.GONE);

            if (roomItem.equipment.contains("2 Laptops"))
                mLaptopsView.setVisibility(View.VISIBLE);
            else mLaptopsView.setVisibility(View.GONE);

            if (roomItem.equipment.contains("Interactive Whiteboard"))
                mWhiteboardView.setVisibility(View.VISIBLE);
            else mWhiteboardView.setVisibility(View.GONE);

            if (roomItem.equipment.contains("Projector"))
                mProjectorView.setVisibility(View.VISIBLE);
            else mProjectorView.setVisibility(View.GONE);

            if (roomItem.equipment.contains("Fridge with Softdrinks"))
                mFridgeView.setVisibility(View.VISIBLE);
            else mFridgeView.setVisibility(View.GONE);

            if (roomItem.equipment.contains("Big Lego Set"))
                mLegoSetView.setVisibility(View.VISIBLE);
            else mLegoSetView.setVisibility(View.GONE);

            if (roomItem.equipment.contains("Hi-Fi System"))
                mHifiSystemView.setVisibility(View.VISIBLE);
            else mHifiSystemView.setVisibility(View.GONE);

            if (roomItem.equipment.contains("50\\\" Flatscreen"))
                mFlatScreenView.setVisibility(View.VISIBLE);
            else mFlatScreenView.setVisibility(View.GONE);
        }

        @OnClick(R.id.room_item)
        void onParkingItemClicked() {
            mItemClickListener.onRoomItemClick(getAdapterPosition(), mRoomList.get(getAdapterPosition()));
        }
    }
}
