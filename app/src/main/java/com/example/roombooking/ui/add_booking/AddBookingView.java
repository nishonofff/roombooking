package com.example.roombooking.ui.add_booking;

import com.arellomobile.mvp.MvpView;
import com.example.roombooking.data.network.entity.WorkspaceResponse;

import java.util.List;

public interface AddBookingView extends MvpView {

    void showNoInternetConnection();

    void addBookingSuccess();

    void addBookingError(String error);

    void showProgress();

    void hideProgress();


}
