package com.example.roombooking.ui.add_booking;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.AttendeeItem;
import com.example.roombooking.ui.widgets.adapters.AttendeeAdapter;
import com.example.roombooking.ui.widgets.dialogs.AlertDialogs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;


/**
 * The type Add booking activity.
 */
public class AddBookingActivity extends MvpAppCompatActivity implements
        AttendeeAdapter.AttendeeItemClickListener, AddBookingView {


    @BindView(R.id.attendees_recycler_view)
    RecyclerView mAttendeeRecyclerView;

    @BindView(R.id.start_time_edit_text)
    EditText mStartTime;

    @BindView(R.id.end_time_edit_text)
    EditText mEndTime;

    @BindView(R.id.event_title)
    EditText mTitleView;

    @BindView(R.id.event_description)
    EditText mDescriptionView;

    @BindView(R.id.room_number)
    EditText mRoomNumberView;

    /**
     * Add booking presenter.
     */
    @InjectPresenter
    AddBookingPresenter mAddBookingPresenter;

    private EditText mAttendeeName;
    private EditText mAttendeeEmail;
    private EditText mAttendeePhoneNumber;

    private AttendeeAdapter mAttendeeAdapter;
    private AlertDialog mAlertDialog;

    private List<AttendeeItem> mAttendeeItemList = new ArrayList<>();

    private String mDate, mWorkspace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_booking);

        ButterKnife.bind(this);

        mDate = getIntent().getStringExtra("DATE");
        mWorkspace = getIntent().getStringExtra("WORKSPACE");

        mAttendeeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAttendeeAdapter = new AttendeeAdapter(this);
        mAttendeeAdapter.setItemClickListener(this);
        mAttendeeAdapter.setAttendeeList(mAttendeeItemList);
        mAttendeeRecyclerView.setAdapter(mAttendeeAdapter);
    }

    /**
     * On reserve clicked.
     */
    @OnClick(R.id.reserve_button)
    void onReserveClicked() {
        mAddBookingPresenter.addBooking(mWorkspace, mDate, mStartTime.getText().toString(), mEndTime.getText().toString(), mTitleView.getText().toString(), mDescriptionView.getText().toString(), mRoomNumberView.getText().toString(), mAttendeeItemList);
    }

    /**
     * On back button clicked.
     */
    @OnClick(R.id.toolbar_back_button)
    void onBackButtonClicked() {
        onBackPressed();
    }

    /**
     * On start time clicked.
     */
    @OnClick(R.id.start_time_edit_text)
    void onStartTimeClicked() {
        showTimePickerDialog(mStartTime);
    }

    /**
     * On end time clicked.
     */
    @OnClick(R.id.end_time_edit_text)
    void onEndTimeClicked() {
        showTimePickerDialog(mEndTime);
    }

    /**
     * On add attendee clicked.
     */
    @OnClick(R.id.fab_add_attendee)
    void onAddAttendeeClicked() {

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_attendee_view, null);

        mAttendeeName = dialogView.findViewById(R.id.attendee_name);
        mAttendeeEmail = dialogView.findViewById(R.id.attendee_email);
        mAttendeePhoneNumber = dialogView.findViewById(R.id.attendee_phone_number);

        mAlertDialog = new AlertDialog.Builder(this, R.style.AlertDialog)
                .setTitle(getResources().getString(R.string.title_add_attendee))
                .setPositiveButton(getResources().getString(R.string.txt_add), (dialog, which) -> {
                    AttendeeItem item = new AttendeeItem();

                    String name = mAttendeeName.getText().toString();
                    String email = mAttendeeEmail.getText().toString();
                    String phoneNumber = mAttendeePhoneNumber.getText().toString();

                    if (name.isEmpty()) {
                        mAttendeeName.setError(getResources().getString(R.string.msg_input_name_field));
                        return;
                    }
                    if (email.isEmpty()) {
                        mAttendeeEmail.setError(getResources().getString(R.string.msg_input_email_field));
                        return;
                    }
                    if (phoneNumber.isEmpty()) {
                        mAttendeePhoneNumber.setError(getResources().getString(R.string.msg_input_number_field));
                        return;
                    }
                    item.name = mAttendeeName.getText().toString();
                    item.email = mAttendeeEmail.getText().toString();
                    String formattedNumber = "";
                    if (phoneNumber.startsWith("+"))
                        formattedNumber = phoneNumber;
                    else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            formattedNumber = PhoneNumberUtils.formatNumberToE164(phoneNumber, "DE");
                        }
                    }
                    item.phoneNumber = formattedNumber;

                    mAttendeeItemList.add(item);
                    mAttendeeAdapter.setAttendeeList(mAttendeeItemList);
                    mAttendeeAdapter.notifyItemInserted(mAttendeeItemList.size());
                })
                .setNegativeButton("Cancel", null)
                .setView(dialogView).create();

        mAlertDialog.show();
    }

    private void showTimePickerDialog(EditText editText) {
        final Calendar myCalender = Calendar.getInstance();
        int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        int minute = myCalender.get(Calendar.MINUTE);


        @SuppressLint("DefaultLocale")
        TimePickerDialog.OnTimeSetListener myTimeListener = (view, hourOfDay, minute1) -> {
            if (view.isShown()) {
                myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalender.set(Calendar.MINUTE, minute1);
                editText.setText(String.format(String.format("%s:%s", String.valueOf(hourOfDay), String.valueOf(minute1))));
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
        timePickerDialog.setTitle("Choose hour:");
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();
    }

    @Override
    public void onDeleteAttendee(int position, AttendeeItem attendeeItem) {
        AlertDialogs.deleteDialog(this, (dialog, which) -> {
            mAttendeeItemList.remove(attendeeItem);
            mAttendeeAdapter.setAttendeeList(mAttendeeItemList);
            mAttendeeAdapter.notifyItemRemoved(position);
        }, null);
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showErrorDialog(this, getResources().getString(R.string.msg_error));
    }

    @Override
    public void addBookingSuccess() {
        AlertDialogs.showSuccessDialog(this, getResources().getString(R.string.msg_booking_success));
    }

    @Override
    public void addBookingError(String error) {
        AlertDialogs.showErrorDialog(this, error);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }
}
