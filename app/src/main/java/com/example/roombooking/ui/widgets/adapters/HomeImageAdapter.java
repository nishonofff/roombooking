package com.example.roombooking.ui.widgets.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.roombooking.utils.AppConstants.BASE_URL_PATH;

public class HomeImageAdapter extends PagerAdapter {

    @BindView(R.id.workspace_title)
    TextView mWorkspace;
    @BindView(R.id.workspace_item_image)
    RoundedImageView mCardImageView;

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private HomeEventsClickListener mHomeEventClickListener;
    private int mItemPosition;
    private List<WorkspaceResponse.Workspace> mWorkspaceList;

    public HomeImageAdapter(Context context) {
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void setHomeEventClickListener(HomeEventsClickListener homeEventClickListener) {
        this.mHomeEventClickListener = homeEventClickListener;
    }

    public void setwWorkspaceList(List<WorkspaceResponse.Workspace> workspaceList) {
        mWorkspaceList = workspaceList;
    }

    @Override
    public int getCount() {
        if (mWorkspaceList != null)
            return mWorkspaceList.size();
        else return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.workspace_item, container, false);

        ButterKnife.bind(this, itemView);

        WorkspaceResponse.Workspace workspaceItem = mWorkspaceList.get(position);
        mWorkspace.setText(workspaceItem.title);

        Picasso.with(mContext)
                .load(BASE_URL_PATH + workspaceItem.icon)
                .into(mCardImageView);

        container.addView(itemView, 0);
        return itemView;
    }


    public interface HomeEventsClickListener {
        void onHomeEventItemClick(int position);
    }
}
