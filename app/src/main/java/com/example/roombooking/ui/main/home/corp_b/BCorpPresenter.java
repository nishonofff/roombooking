package com.example.roombooking.ui.main.home.corp_b;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.roombooking.App;
import com.example.roombooking.data.network.ApiService;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.utils.AppUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

@InjectViewState
public class BCorpPresenter extends MvpPresenter<BCorpView> {

    private static final String TAG = BCorpPresenter.class.getName();

    /**
     * The api service.
     */
    @Inject
    ApiService mApiService;

    /**
     * Instantiates a new B corp presenter.
     */
    BCorpPresenter() {
        App.getAppComponent().inject(this);
    }


    /**
     * Gets room list.
     *
     * @param date the date
     */
    @SuppressLint("CheckResult")
    void getRoomList(String date) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            return;
        }

        getViewState().showProgress();

        Map<String, Object> body = new HashMap<>();
        if (date.equals("today"))
            body.put("date", date);
        else {
            body.put("date", AppUtils.convertDateToUnitStamp(date));
        }

        mApiService.getRoomsFromBCorp(body).subscribe((List<RoomResponse.Room> response) -> {
                    getViewState().hideProgress();
                    getViewState().setRoomList(response);
                },
                (Throwable throwable) -> {
                    getViewState().hideProgress();
                    getViewState().loadRoomListError();
                });
    }

}
