package com.example.roombooking.ui.main.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;
import com.example.roombooking.ui.main.home.corp_a.ACorpFragment;
import com.example.roombooking.ui.main.home.corp_b.BCorpFragment;
import com.example.roombooking.ui.widgets.adapters.HomeImageAdapter;
import com.example.roombooking.ui.widgets.dialogs.AlertDialogs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.roombooking.utils.AppUtils.convertDpToPx;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class HomeFragment extends MvpAppCompatFragment implements ViewPager.OnPageChangeListener, HomeView {

    public static final String TAG = HomeFragment.class.getName();

    @BindView(R.id.tab_view_pager)
    ViewPager mViewPager;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @InjectPresenter
    HomePresenter mHomePresenter;

    private HomeImageAdapter mAdapter;
    private FragmentManager mFragmentManager;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHomePresenter.getWorkspaces();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mFragmentManager = getChildFragmentManager();

        mAdapter = new HomeImageAdapter(getActivity());
    }

    public void onACorpClick() {
        mFragmentManager.beginTransaction()
                .replace(R.id.container, ACorpFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    public void onBCorpClick() {
        mFragmentManager.beginTransaction()
                .replace(R.id.container, BCorpFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        if (i == 0) {
            onACorpClick();
        } else {
            onBCorpClick();
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showErrorDialog(getActivity(), getResources().getString(R.string.msg_error));
    }

    @Override
    public void setWorkspaces(String workspaces, List<WorkspaceResponse.Workspace> workspaceList) {
        if (workspaces.equals("AB")) {
            mAdapter.setwWorkspaceList(workspaceList);
            onACorpClick();
        } else if (workspaces.equals("A")) {
            mAdapter.setwWorkspaceList(workspaceList.subList(0, 1));
            onACorpClick();
        } else {
            mAdapter.setwWorkspaceList(workspaceList.subList(1, 2));
            onBCorpClick();
        }

        mViewPager.setAdapter(mAdapter);
        mViewPager.setPadding(100, 0, 100, 0);
        mViewPager.setPageMargin(convertDpToPx(getActivity(), 14));

        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    public void loadRoomListError() {
        AlertDialogs.showErrorDialog(getActivity(), getResources().getString(R.string.msg_error));
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }
}
