package com.example.roombooking.ui.main;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.roombooking.R;
import com.example.roombooking.ui.main.bookings.FindRoomFragment;
import com.example.roombooking.ui.main.home.HomeFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.bottom_navbar_home)
    ImageView mNavbarHome;
    @BindView(R.id.bottom_navbar_search)
    ImageView mNavbarSearch;
    @BindView(R.id.bottom_navbar_rooms)
    ImageView mNavbarRooms;

    @BindView(R.id.home_line)
    View mHomeLine;
    @BindView(R.id.search_line)
    View mSearchLine;
    @BindView(R.id.rooms_line)
    View mRoomsLine;

    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mFragmentManager = getSupportFragmentManager();

        onHomeClick();
    }

    @OnClick(R.id.bottom_navbar_home)
    public void onHomeClick() {
        clearSelectionBottomIcons();
        mHomeLine.setVisibility(View.VISIBLE);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, HomeFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_search)
    public void onSearchClick() {
        clearSelectionBottomIcons();
        mSearchLine.setVisibility(View.VISIBLE);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, HomeFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_rooms)
    public void onRoomsClick() {
        clearSelectionBottomIcons();
        mRoomsLine.setVisibility(View.VISIBLE);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, FindRoomFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    public void clearSelectionBottomIcons() {
        mHomeLine.setVisibility(View.GONE);
        mSearchLine.setVisibility(View.GONE);
        mRoomsLine.setVisibility(View.GONE);
    }
}
