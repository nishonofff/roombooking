package com.example.roombooking.ui.main.home.corp_b;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.ui.main.home.room_info.RoomInfoActivity;
import com.example.roombooking.ui.widgets.adapters.RoomsAdapter;
import com.example.roombooking.ui.widgets.dialogs.AlertDialogs;
import com.example.roombooking.utils.AppUtils;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */
public class BCorpFragment extends MvpAppCompatFragment implements RoomsAdapter.RoomItemClickListener, BCorpView {

    public static final String TAG = BCorpFragment.class.getName();

    @BindView(R.id.rooms_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.date_text_view)
    TextView mDateTextView;

    @BindView(R.id.search_edit_text)
    EditText mSearchView;

    /**
     * The b corp presenter.
     */
    @InjectPresenter
    BCorpPresenter mBCorpPresenter;

    private RoomsAdapter mAdapter;

    private List<RoomResponse.Room> mRoomList;
    private List<RoomResponse.Room> mFilteredRoomList = new ArrayList<>();

    private DatePickerDialog mDatePicker;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    /**
     * New instance b corp fragment.
     *
     * @return the b corp fragment
     */
    public static BCorpFragment newInstance() {
        BCorpFragment fragment = new BCorpFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBCorpPresenter.getRoomList("today");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_b_corp, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0)
                    mAdapter.setRoomList(mRoomList);
                else {
                    mFilteredRoomList.clear();
                    for (RoomResponse.Room room : mRoomList) {
                        if (room.name.contains(s))
                            mFilteredRoomList.add(room);
                    }
                    mAdapter.setRoomList(mFilteredRoomList);
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        mAdapter = new RoomsAdapter(getActivity());
        mAdapter.setItemClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        setUpDatePickerDialog();
    }

    /**
     * On date edited.
     */
    @SuppressLint("DefaultLocale")
    @OnClick(R.id.date_text_view)
    void onDateEdited() {
        mDateSetListener = (view, year, monthOfYear, dayOfMonth) -> {
            mDateTextView.setText(String.format("%d/%d/%d", dayOfMonth, monthOfYear + 1, year));
            mBCorpPresenter.getRoomList(String.format("%d/%d/%d", dayOfMonth, monthOfYear + 1, year));
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mDatePicker.setOnDateSetListener(mDateSetListener);
        }

        mDatePicker.show();
    }

    /**
     * On available change.
     *
     * @param checked the checked
     */
    @OnCheckedChanged(R.id.available_switch)
    void onAvailableChange(boolean checked) {
        String currentTime = AppUtils.getTime();
        if (checked) {
            mFilteredRoomList.clear();
            for (RoomResponse.Room room : mRoomList) {
                for (int i = 0; i < room.availableTimes.size(); i++) {
                    String[] times = room.availableTimes.get(i).split("-");

                    Map<String, Integer> result = AppUtils.getTimeDifference(times[0], currentTime);
                    if (result.get("hours") >= 0 || result.get("mins") >= 0) {
                        Map<String, Integer> secondResult = AppUtils.getTimeDifference(currentTime, times[1]);

                        if (secondResult.get("hours") >= 1) {
                            mFilteredRoomList.add(room);
                        }
                    }
                }
            }

            mAdapter.setRoomList(mFilteredRoomList);
            mAdapter.notifyDataSetChanged();
        } else {
            mAdapter.setRoomList(mRoomList);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRoomItemClick(int position, RoomResponse.Room roomItem) {
        Intent intent = new Intent(getActivity(), RoomInfoActivity.class);
        roomItem.workspace = "B";
        intent.putExtra("DATE", mDateTextView.getText().toString());
        intent.putExtra("ROOM_ITEM", roomItem);
        startActivity(intent);
    }

    @Override
    public void showNoInternetConnection() {
        AlertDialogs.showErrorDialog(getActivity(), getResources().getString(R.string.msg_error));
    }

    @Override
    public void setRoomList(List<RoomResponse.Room> roomList) {
        mRoomList = roomList;
        mAdapter.setRoomList(roomList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void loadRoomListError() {
        AlertDialogs.showErrorDialog(getActivity(), getResources().getString(R.string.msg_error));
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    /**
     * Sets up date picker dialog.
     */
    void setUpDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        mDatePicker = new DatePickerDialog(getActivity()
                , android.R.style.Theme_Holo_Light_Dialog_MinWidth
                , mDateSetListener
                , year, month, day);
        mDatePicker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
