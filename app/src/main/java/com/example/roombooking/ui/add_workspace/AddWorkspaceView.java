package com.example.roombooking.ui.add_workspace;

import com.arellomobile.mvp.MvpView;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;

import java.util.List;

public interface AddWorkspaceView extends MvpView {

    void showNoInternetConnection();

    void setWorkspaceList(List<WorkspaceResponse.Workspace> workspaceList);

    void loadRoomListError();

    void showProgress();

    void hideProgress();


}
