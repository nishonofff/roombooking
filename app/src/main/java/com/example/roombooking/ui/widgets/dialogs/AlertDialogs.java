package com.example.roombooking.ui.widgets.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.example.roombooking.R;

public class AlertDialogs {

    public static void showNoConnectionMessage(Context context, DialogInterface.OnClickListener positiveListener,
                                               DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(false)
                .setMessage("Oops ! Something went wrong. Please check your connection and try again.")
                .setPositiveButton("Retry", positiveListener)
                .setNegativeButton(android.R.string.cancel, negativeListener)
                .show();
    }

    public static void deleteDialog(Context context, DialogInterface.OnClickListener positiveListener,
                                     DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(false)
                .setMessage("Are you sure you want to delete this user?")
                .setPositiveButton("Delete", positiveListener)
                .setNegativeButton(android.R.string.cancel, negativeListener)
                .show();
    }


    public static void showSuccessDialog(Context context) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setMessage("Your order has been sent successfully !")
                .show();
    }

    public static void showSuccessDialog(Context context, String message) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setMessage(message)
                .show();
    }

    public static void showErrorDialog(Context context) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setTitle("Error !")
                .setMessage("Oops ! Something went wrong. Please check your connection and try again.")
                .show();
    }

    public static void showErrorDialog(Context context, String message) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setTitle("Error !")
                .setMessage(message)
                .show();
    }

}
