package com.example.roombooking.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.AttendeeItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnLongClick;

public class AttendeeAdapter extends RecyclerView.Adapter<AttendeeAdapter.ViewHolder> {

    private Context mContext;

    private static AttendeeItemClickListener mItemClickListener;
    private List<AttendeeItem> mAttendeeList;

    public AttendeeAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(AttendeeItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setAttendeeList(List<AttendeeItem> attendeeList) {
        mAttendeeList = attendeeList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.attendee_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setAttendeeItem(mAttendeeList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mAttendeeList != null)
            return mAttendeeList.size();
        else return 0;
    }

    public interface AttendeeItemClickListener {
        void onDeleteAttendee(int position, AttendeeItem attendeeItem);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.attendee_name)
        TextView mAttendeeName;
        @BindView(R.id.attendee_email)
        TextView mAttendeeEmail;
        @BindView(R.id.attendee_phone_number)
        TextView mAttendeePhoneNumber;

        AttendeeItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, AttendeeItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setAttendeeItem(AttendeeItem attendeeItem) {
            mAttendeeName.setText(attendeeItem.name);
            mAttendeeEmail.setText(attendeeItem.email);
            mAttendeePhoneNumber.setText(attendeeItem.phoneNumber);
        }

        @OnLongClick(R.id.attendee_item)
        boolean onDeleteAttendee() {
            mItemClickListener.onDeleteAttendee(getAdapterPosition(), mAttendeeList.get(getAdapterPosition()));
            return true;
        }

    }

}
