package com.example.roombooking.ui.widgets.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.roombooking.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.roombooking.utils.AppConstants.BASE_URL_PATH;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private Context mContext;
    private static ImageAdapter.ImageItemClickListener mItemClickListener;
    private List<String> mImageList;

    public ImageAdapter(Context context) {
        mContext = context;
    }

    public void setImageList(List<String> imageList) {
        mImageList = imageList;
    }

    public void setItemClickListener(ImageAdapter.ImageItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.image_item, parent, false);
        return new ViewHolder(view, mItemClickListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.setImageItem(mImageList.get(position));
    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    public interface ImageItemClickListener {
        void onImageItemClick(int position, String imageURL, View view);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_image_view)
        ImageView mImageView;

        private ImageItemClickListener mItemClickListener;


        public ViewHolder(View itemView, ImageItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setImageItem(String imageURL) {
            Picasso.with(mContext)
                    .load(BASE_URL_PATH + imageURL)
                    .into(mImageView);
        }

        @OnClick(R.id.item_image_view)
        void onImageItemClicked(View view) {
            mItemClickListener.onImageItemClick(getAdapterPosition(), mImageList.get(getAdapterPosition()), view);
        }

    }
}
