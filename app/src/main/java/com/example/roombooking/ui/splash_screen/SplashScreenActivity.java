package com.example.roombooking.ui.splash_screen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.roombooking.data.prefs.PrefsService;
import com.example.roombooking.ui.add_workspace.AddWorkspaceActivity;
import com.example.roombooking.ui.main.MainActivity;
import com.example.roombooking.R;
import com.example.roombooking.ui.widgets.dialogs.AlertDialogs;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class SplashScreenActivity extends MvpAppCompatActivity implements SplashScreenView {

    int SPLASH_SCREEN_DURATION = 2000;
    private Intent mIntent;

    @InjectPresenter
    SplashScreenPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mPresenter.checkUserStatus();
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void setUserStatus(String workspaces) {
        if (workspaces.equals("0"))
            mIntent = new Intent(this, AddWorkspaceActivity.class);
        else
            mIntent = new Intent(this, MainActivity.class);

        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        new Handler().postDelayed(() -> {
            if (mIntent != null) {
                startActivity(mIntent);
            }
        }, SPLASH_SCREEN_DURATION);
    }
}
