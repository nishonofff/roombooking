package com.example.roombooking.ui.main.home.room_info;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.roombooking.R;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.ui.add_booking.AddBookingActivity;
import com.example.roombooking.ui.widgets.adapters.ImageAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.ehsun.coloredtimebar.TimelineView;

public class RoomInfoActivity extends AppCompatActivity implements ImageAdapter.ImageItemClickListener {

    @BindView(R.id.image_recycler_view)
    RecyclerView mImageRecyclerView;

    @BindView(R.id.timeline_view)
    TimelineView mTimeLineView;

    @BindView(R.id.room_address)
    TextView mRoomAddress;
    @BindView(R.id.room_title)
    TextView mRoomTitle;
    @BindView(R.id.room_size)
    TextView mRoomSize;
    @BindView(R.id.room_capacity)
    TextView mRoomCapacity;

    @BindView(R.id.laptop_view)
    View mLaptopView;
    @BindView(R.id.laptops_view)
    View mLaptopsView;
    @BindView(R.id.fridge_view)
    View mFridgeView;
    @BindView(R.id.hifi_system_view)
    View mHifiSystemView;
    @BindView(R.id.projector_view)
    View mProjectorView;
    @BindView(R.id.flat_screen_view)
    View mFlatScreenView;
    @BindView(R.id.lego_set_view)
    View mLegoSetView;
    @BindView(R.id.whiteboard_view)
    View mWhiteboardView;

    private ImageAdapter mImageAdapter;
    public static final String ROOM_IMAGE_KEY = "ROOM_IMAGE_KEY";
    private RoomResponse.Room mRoomItem;
    private String mDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_info);

        ButterKnife.bind(this);

        mRoomItem = (RoomResponse.Room) getIntent().getSerializableExtra("ROOM_ITEM");
        mDate = getIntent().getStringExtra("DATE");
        mImageRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImageAdapter = new ImageAdapter(this);
        mImageAdapter.setItemClickListener(this);
        mImageAdapter.setImageList(mRoomItem.images);
        mImageRecyclerView.setAdapter(mImageAdapter);

        setRoomInfo(mRoomItem);
    }

    private void setRoomInfo(RoomResponse.Room roomItem) {
        if (roomItem.equipment.contains("Laptop")) {
            mLaptopView.setVisibility(View.VISIBLE);
        }
        if (roomItem.equipment.contains("2 Laptops")) {
            mLaptopsView.setVisibility(View.VISIBLE);
        }
        if (roomItem.equipment.contains("Interactive Whiteboard")) {
            mWhiteboardView.setVisibility(View.VISIBLE);
        }
        if (roomItem.equipment.contains("Projector")) {
            mProjectorView.setVisibility(View.VISIBLE);
        }
        if (roomItem.equipment.contains("Fridge with Softdrinks")) {
            mFridgeView.setVisibility(View.VISIBLE);
        }
        if (roomItem.equipment.contains("Big Lego Set")) {
            mLegoSetView.setVisibility(View.VISIBLE);
        }
        if (roomItem.equipment.contains("Hi-Fi System")) {
            mHifiSystemView.setVisibility(View.VISIBLE);
        }
        if (roomItem.equipment.contains("50\\\" Flatscreen")) {
            mFlatScreenView.setVisibility(View.VISIBLE);
        }

        mRoomTitle.setText(String.format("Room %s", roomItem.name));
        mRoomAddress.setText(roomItem.location);
        mRoomSize.setText(roomItem.size);
        mRoomCapacity.setText(String.format("%s people", String.valueOf(roomItem.capacity)));

        List<String> list = new ArrayList<>();

        for (String availableTime : roomItem.availableTimes) {
            list.add(availableTime);
        }
        mTimeLineView.setAvailableTimeRange(list);
    }

    @OnClick(R.id.toolbar_back_button)
    void onBackButtonClicked() {
        onBackPressed();
    }

    @OnClick(R.id.reserve_button)
    void onReserveButtonClicked() {
        Intent intent = new Intent(this, AddBookingActivity.class);
        intent.putExtra("WORKSPACE", mRoomItem.workspace);
        intent.putExtra("DATE", mDate);
        startActivity(intent);
    }

    @Override
    public void onImageItemClick(int position, String imageURL, View view) {
        Intent intent = new Intent(this, ShowImageActivity.class);
        intent.putExtra(ROOM_IMAGE_KEY, imageURL);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, ViewCompat.getTransitionName(view));
        startActivity(intent, options.toBundle());
    }
}
