package com.example.roombooking.data.network.entity;

import com.squareup.moshi.Json;

public class BookingResponse {
    public boolean success;

    public Error error;

    public static class Error {
        public String text;
        public String code;
    }
}
