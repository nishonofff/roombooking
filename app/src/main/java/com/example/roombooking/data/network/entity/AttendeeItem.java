package com.example.roombooking.data.network.entity;

public class AttendeeItem {
    public String name;
    public String email;
    public String phoneNumber;
}
