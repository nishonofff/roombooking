package com.example.roombooking.data.prefs;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface PrefsService {

    String LOCALE = "locale";

    void clearPrefs();

    String getLocale();

    void setLocale(String locale);

    String getWorkspace(String key);

    void setWorkspace(String key, String workpace);

    void clearUserData();

}
