package com.example.roombooking.data.prefs;

import android.content.Context;

import com.example.roombooking.utils.AppConstants;


/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class PrefsServiceImpl implements PrefsService, AppConstants {

    private final SecuredPreferencesHelper mSecuredPrefs;

    public PrefsServiceImpl(Context context, String prefsFileName) {
        mSecuredPrefs = new SecuredPreferencesHelper(context, prefsFileName);
    }

    @Override
    public void clearPrefs() {
        mSecuredPrefs.clearAllData();
    }

    @Override
    public String getLocale() {
        return mSecuredPrefs.get(LOCALE, "ru");
    }

    @Override
    public void setLocale(String locale) {
        mSecuredPrefs.put(LOCALE, locale);
    }

    @Override
    public String getWorkspace(String key) {
        return mSecuredPrefs.get(key, "0");
    }

    @Override
    public void setWorkspace(String key, String workspace) {
        mSecuredPrefs.put(key, workspace);
    }

    @Override
    public void clearUserData() {
        mSecuredPrefs.deleteSavedData("token");
    }
}
