package com.example.roombooking.data.network.entity;

import com.squareup.moshi.Json;

import java.io.Serializable;
import java.util.List;

public class RoomResponse extends BaseResponse implements Serializable {

    @Json(name = "result")
    public Room result;

    public static class Room implements Serializable {
        @Json(name = "name")
        public String name;
        @Json(name = "location")
        public String location;
        @Json(name = "equipment")
        public List<String> equipment;
        @Json(name = "size")
        public String size;
        @Json(name = "capacity")
        public int capacity;
        @Json(name = "avail")
        public List<String> availableTimes;
        @Json(name = "images")
        public List<String> images;

        public String workspace;
    }

}
