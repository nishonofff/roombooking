package com.example.roombooking.data.network.entity;

import com.squareup.moshi.Json;

import java.util.List;

public class WorkspaceResponse extends BaseResponse {

    public static class Workspace {
        @Json(name = "title")
        public String title;
        @Json(name = "icon")
        public String icon;
    }

}
