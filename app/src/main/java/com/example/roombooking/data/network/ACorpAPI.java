package com.example.roombooking.data.network;


import com.example.roombooking.data.network.entity.BookingResponse;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public interface ACorpAPI {

    /**
     * Add booking
     */
    @POST("sendpass")
    Single<BookingResponse> addBooking(@Body Map<String, Object> body);

    /**
     * Gets workspaces
     */
    @POST("workspace")
    Single<WorkspaceResponse.Workspace> getWorkspace();

    /**
     * Gets rooms
     */
    @POST("getrooms")
    Single<List<RoomResponse.Room>> getRooms(@Body Map<String, Object> body);

}
