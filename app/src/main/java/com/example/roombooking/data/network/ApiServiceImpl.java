package com.example.roombooking.data.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;

import com.example.roombooking.data.network.entity.BookingResponse;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class ApiServiceImpl implements ApiService {

    private final Context mContext;
    private final ACorpAPI mACorpAPI;
    private final BCorpAPI mBCorpAPI;
    private String mLocale;

    public ApiServiceImpl(Context context, ACorpAPI ACorpAPI, BCorpAPI BCorpAPI) {
        mContext = context;
        mACorpAPI = ACorpAPI;
        mBCorpAPI = BCorpAPI;
    }

    @Override
    public void setLocale(String locale) {
        mLocale = locale;
    }

    @Override
    public boolean noConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        Network network = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            network = cm.getActiveNetwork();
            NetworkCapabilities capabilities = cm.getNetworkCapabilities(network);
            if (network != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
                    return !capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
                    return !capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR);
            }
        }
        return true;
    }

    @Override
    public Single<BookingResponse> addBookingFromACorp(Map<String, Object> body) {
        return mACorpAPI.addBooking(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<BookingResponse> addBookingFromBCorp(Map<String, Object> body) {
        return mBCorpAPI.addBooking(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<WorkspaceResponse.Workspace> getWorkspaceFromACorp() {
        return mACorpAPI.getWorkspace()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<WorkspaceResponse.Workspace> getWorkspaceFromBCorp() {
        return mBCorpAPI.getWorkspace()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Single<List<RoomResponse.Room>> getRoomsFromACorp(Map<String, Object> body) {
        return mACorpAPI.getRooms(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<List<RoomResponse.Room>> getRoomsFromBCorp(Map<String, Object> body) {
        return mBCorpAPI.getRooms(body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
