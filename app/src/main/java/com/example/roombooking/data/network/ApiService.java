package com.example.roombooking.data.network;

import com.example.roombooking.data.network.entity.BookingResponse;
import com.example.roombooking.data.network.entity.RoomResponse;
import com.example.roombooking.data.network.entity.WorkspaceResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface ApiService {
    ///if app language will be needed
    void setLocale(String locale);

    /**
     * Checking internet connection
     *
     * @return true if there is not internet connection, otherwise false
     */
    boolean noConnection();

    Single<BookingResponse> addBookingFromACorp(Map<String, Object> body);

    Single<BookingResponse> addBookingFromBCorp(Map<String, Object> body);

    Single<WorkspaceResponse.Workspace> getWorkspaceFromACorp();

    Single<WorkspaceResponse.Workspace> getWorkspaceFromBCorp();

    Single<List<RoomResponse.Room>> getRoomsFromACorp(Map<String, Object> body);

    Single<List<RoomResponse.Room>> getRoomsFromBCorp(Map<String, Object> body);


}
